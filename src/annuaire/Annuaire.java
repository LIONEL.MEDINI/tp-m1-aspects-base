package annuaire;

import java.util.ArrayList;
import java.util.Iterator;

import annuaire.metier.Site;
import annuaire.metier.SiteDAO;
import annuaire.metier.SiteXMLDAO;

public class Annuaire {

    ArrayList<Site> sites = new ArrayList<Site>();
    SiteDAO dao;

    public Annuaire(String d) {
        dao = new SiteXMLDAO(d);
    }

    public void addSite(String desc, String url) {
        Site s = new Site(desc, url);

        // ajout dans la liste
        try {
        	dao.addSite(s);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // ajout dans le support de persistance
        sites.add(s);
    }

    public void removeSite(String desc, String url) {
        Site s = new Site(desc, url);

        // suppression dans la liste
        for (Iterator<Site> i = sites.iterator(); i.hasNext();) {
            Site temp = (Site) i.next();
            if (temp.equals(s)) {
                sites.remove(temp);
                removeSite(desc, url);
                return;
            }
        }

        // suppression dans le support de persistance
        try {
        	dao.deleteSite(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String listSites() {
        String ls = new String();
        for (Iterator<Site> i = sites.iterator(); i.hasNext();) {
            Site s = (Site) i.next();
            ls += "Description :\t" + s.getDescription() + "\n";
            ls += "URL :\t" + s.getURL() + "\n";
        }
        return ls;
    }

    public void initSites() {
        // synchronisation de la liste et du support de persistance
    	ArrayList<Site> temp = dao.getAllSites();
    	sites.clear();
    	for(Site s:temp) {
    		sites.add(new Site(s.getDescription(), s.getURL()));
    	}
    }
}